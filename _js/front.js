;(function ($) {

'use strict';

/*========================================
=            Background Video            =
========================================*/

function backVideoLoad() {
    var slider = new MasterSlider();

    slider.setup('masterslider', {
        width: 960,
        height: 540,
        // autoHeight: true,
        loop: true,
        autoplay: true,
        speed: 2,
        // instantStartLayers: true,
        view: 'fade',
        layout: 'fullscreen',
        grabCursor: false,
        swipe: false,
        mouse: false
    });
}

/*=====  End of Background Video  ======*/

/*=======================================================
=                   Front Scroll Full                   =
=======================================================*/

    // Init controller
    var controller = new ScrollMagic.Controller({
        globalSceneOptions: {
            duration: $('section').height(),
            triggerHook: '.025',
            reverse: true
        },
        vertical: false
    });

    /*
    object to hold href values of links inside our nav with
    the class '.section-nav'

    scene_object = {
    '[scene-name]' : {
        '[target-scene-id]' : '[anchor-href-value]'
    }
    }
    */
    var scenes = {
        'intro': {
            'section_0': 'anchor_0'
        },
        'scene2': {
            'section_1': 'anchor_1'
        },
        'scene3': {
            'section_2': 'anchor_2'
        },
        'scene4': {
            'section_3': 'anchor_3'
        }
    };

    for (var key in scenes) {
        // skip loop if the property is from prototype
        if (!scenes.hasOwnProperty(key)) continue;

        var obj = scenes[key];

        for (var prop in obj) {
            // skip loop if the property is from prototype
            if (!obj.hasOwnProperty(prop)) continue;

            new ScrollMagic.Scene({ triggerElement: '#' + prop })
                .setClassToggle('#' + obj[prop], 'active')
                .addTo(controller);
        }
    }

    // Change behaviour of controller
    // to animate scroll instead of jump
    controller.scrollTo(function (target) {

        TweenMax.to(window, 0.5, {
            scrollTo: {
                x: target,
                autoKill: true // Allow scroll position to change outside itself
            },
            ease: Cubic.easeInOut
        });

    });

    //  Bind scroll to anchor links using Vanilla JavaScript
    var anchor_nav = document.querySelector('.section-nav');

    anchor_nav.addEventListener('click', function (e) {
        var target = e.target,
            id = target.getAttribute('href');

        if (id !== null && id.length > 0) {
            e.preventDefault();
            controller.scrollTo(id);

            if (window.history && window.history.pushState) {
                history.pushState("", document.title, id);
            }
        }
    });

    var $navs = $('.section-nav a');

    window.addEventListener("wheel", onWheel);

    function onWheel(event) {
        // event.addEventListener({ passive: false });
        // event.preventDefault();

        var normalized;
        var delta = event.wheelDelta;
        var scroll = (window.pageXOffset || document.scrollLeft) - (document.clientLeft || 0) || 0;
        var arowDown = $('.arrow-down');

        if (delta) {
            normalized = (delta % 120) == 0 ? delta / 120 : delta / 12;
        } else {
            delta = event.deltaY || event.detail || 0;
            normalized = -(delta % 3 ? delta * 10 : delta / 3);
        }

        var currentIndex = $navs.index($('.active'));
        var newIndex;
        newIndex = normalized > 0 ? currentIndex - 1 : currentIndex + 1;

        if (newIndex >= 0 && newIndex < $navs.length) {
            $navs.eq(newIndex)[0].click();
        }

        arowDown.fadeOut();

    }

/*============  End of Front Scroll Full  =============*/

function initDesktop() {
    backVideoLoad();
    // navMenuOpen();
}

function initMobile() {

}

if (document.body.clientWidth > 420 || screen.width > 420) {
    initDesktop();
} else {
    initMobile();
}

})(jQuery);