;(function ($) {

'use strict';

/*============================================
=                   Cursor                   =
============================================*/

// var bd = document.body,
//     cur = document.getElementById("cursor");
// bd.addEventListener("mousemove", function (n) {
//     (cur.style.left = n.clientX + "px"), (cur.style.top = n.clientY + "px");
// });

/*============  End of Cursor  =============*/

/*===================================
=            Menu                   =
===================================*/

var navToggle = $('.nav-toggle'),
    navMainDesktop = $('#navMainDesktop'),
    navMainDesktopLi = $('#navMainDesktop ul>li'),
    navToggleLine = $('.nav-toggle__line'),
    navToggleLineBefore = $('.nav-toggle__line--before'),
    navToggleLineAfter = $('.nav-toggle__line--after'),
    wrapper = $('.wrapper'),
    navMainMobile = $('#navMainMobile'),
    navMainMobileLi = $('#navMainMobile ul>li'),
    headerMask = $('.header-mask')
;

function navMenuOpen() {

    var tl = new TimelineMax({
        paused: true,
        reversed: true
    });

    tl
        .to(navMainDesktop, 0.4, {
            // zIndex: 9910,
            // autoAlpha: 1,
            visibility: 'visible',
            xPercent: 100,
            ease: Power1.easeInOut
        }, "-=0.8")
        .staggerFrom(navMainDesktopLi, 0.5, {
            xPercent: '-50%',
            // zIndex: 2,
            autoAlpha: 0,
            ease: Circ.easeInOut
        }, "0.1", "-=0.8")
        .to(navToggleLine, 0.4, {
            // autoAlpha: 0,
            rotation: '-45deg',
            scale: 1.1,
            // visibility: 'visible',
            ease: Power1.easeInOut
        }, "-=0.8")
    ;

    /*jshint -W030 */
    navToggle.on('click', function () {
        tl.reversed() ? tl.restart() : tl.reverse();
    });
    // wrapper.on('click', function () {
    //     tl.reverse();
    // });

}

function navMenuOpenMobile() {

    var tl = new TimelineMax({
        paused: true,
        reversed: true
    });

    tl
        .to(navMainMobile, 0.4, {
            // zIndex: 9910,
            // autoAlpha: 1,
            visibility: 'visible',
            xPercent: -100,
            ease: Power1.easeInOut
        }, "-=0.8")
        .staggerFrom(navMainMobileLi, 0.5, {
            xPercent: '50%',
            // zIndex: 2,
            autoAlpha: 0,
            ease: Circ.easeInOut
        }, "0.1", "-=0.8")
        .to(headerMask, 0.4, {
            visibility: 'visible',
            autoAlpha: 1,
            ease: Power1.easeInOut
        }, "-=0.8")
        .to(navToggleLine, 0.4, {
            // autoAlpha: 0,
            rotation: '-45deg',
            scale: 1.1,
            // visibility: 'visible',
            ease: Power1.easeInOut
        }, "-=0.8")
    ;

    /*jshint -W030 */
    navToggle.on('click', function () {
        tl.reversed() ? tl.restart() : tl.reverse();
    });
    headerMask.on('click', function () {
        tl.reverse();
    });

}

/*=====  End of Menu Mobile  ======*/

function initDesktop() {
    navMenuOpen();
}

function initMobile() {
    navMenuOpenMobile();
}

if (document.body.clientWidth > 420 || screen.width > 420) {
    initDesktop();
} else {
    initMobile();
}

})(jQuery);