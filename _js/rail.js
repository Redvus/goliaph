;(function ($) {

'use strict';

/*=================================================
=                   Scroll Drag                   =
=================================================*/

function scrollDrag() {
	var types = ['DOMMouseScroll', 'mousewheel'];

	if ($.event.fixHooks) {
		for (var i = types.length; i;) {
			$.event.fixHooks[types[--i]] = $.event.mouseHooks;
		}
	}

	$.event.special.mousewheel = {
		setup: function () {
			if (this.addEventListener) {
				for (var i = types.length; i;) {
					this.addEventListener(types[--i], handler, false);
				}
			} else {
				this.onmousewheel = handler;
			}
		},

		teardown: function () {
			if (this.removeEventListener) {
				for (var i = types.length; i;) {
					this.removeEventListener(types[--i], handler, false);
				}
			} else {
				this.onmousewheel = null;
			}
		}
	};

	$.fn.extend({
		mousewheel: function (fn) {
			return fn ? this.bind("mousewheel", fn) : this.trigger("mousewheel");
		},

		unmousewheel: function (fn) {
			return this.unbind("mousewheel", fn);
		}
	});


	function handler(event) {
		var orgEvent = event || window.event, args = [].slice.call(arguments, 1), delta = 0, returnValue = true, deltaX = 0, deltaY = 0;
		event = $.event.fix(orgEvent);
		event.type = "mousewheel";

		// Old school scrollwheel delta
		if (orgEvent.wheelDelta) { delta = orgEvent.wheelDelta / 120; }
		if (orgEvent.detail) { delta = -orgEvent.detail / 3; }

		// New school multidimensional scroll (touchpads) deltas
		deltaY = delta;

		// Gecko
		if (orgEvent.axis !== undefined && orgEvent.axis === orgEvent.HORIZONTAL_AXIS) {
			deltaY = 0;
			deltaX = -1 * delta;
		}

		// Webkit
		if (orgEvent.wheelDeltaY !== undefined) { deltaY = orgEvent.wheelDeltaY / 120; }
		if (orgEvent.wheelDeltaX !== undefined) { deltaX = -1 * orgEvent.wheelDeltaX / 120; }

		// Add event and delta to the front of the arguments
		args.unshift(event, delta, deltaX, deltaY);

		/*jshint -W040 */
		return ($.event.dispatch || $.event.handle).apply(this, args);
	}

	// EXTEND jQuery
	$.js = function (el) {
		return $('[data-js=' + el + ']');
	};

	var mainWrapper = $.js('main-wrapper');
	var sectionWrapper = $.js('sections-wrapper');

	var vW = $(window).width();

	mainWrapper.mousewheel(function (event, delta) {
		this.scrollLeft -= (delta * 50);

		event.preventDefault();
	});

	Draggable.create(mainWrapper, {
		type: 'scrollLeft',
		throwProps: false,
		cursor: "grab",
		onDragStart: function () {
			TweenLite.set(mainWrapper, { cursor: "grabbing" });
		}
	});

	function setWidth() {
		var section = $.js('section');
		var totalWidth = 0;

		section.each(function () {
			totalWidth += parseInt($(this).width(), 10);
		});

		sectionWrapper.css('width', '' + (totalWidth + 1) + 'px');
	}

	$(window).on('resize', function () {
		setWidth();
	});

	setWidth();
}

/*============  End of Scroll Drag  =============*/

/*===========================================
=            ScrollMagic Section            =
===========================================*/

var
	$title_0 = $('#sectionTitle_0'),
	$title_1 = $('#sectionTitle_1'),
	$title_2 = $('#sectionTitle_2'),
	$section_0 = $('#section_0')[0],
	$section_1 = $('#section_1')[0],
	$section_2 = $('#section_2')[0],
	$titlePercent = 3,
	$triggerHook = 0.7,
	$titleTime = 0.2
;

var controller = new ScrollMagic.Controller({
	vertical: false
});

/*----------  0 Section  ----------*/
function titleSection_0() {
	var tl = new TimelineMax();
	tl
		.to($title_0, $titleTime, {
			xPercent: -$titlePercent,
			autoAlpha: 0,
			// display: 'n_25',
			ease: Power1.easeInOut
		})
		.fromTo($title_1, $titleTime, {
			xPercent: $titlePercent,
			autoAlpha: 0
		}, {
			xPercent: 0,
			autoAlpha: 1,
			ease: Power1.easeInOut
		})
		// .fromTo($infoSectionText, 0.4, {
		//     yPercent: $titlePercent,
		//     autoAlpha: 0
		// }, {
		//     yPercent: 0,
		//     autoAlpha: 1,
		//     ease: Power1.easeInOut
		// }, '-=1')
		// .set($infoSectionText, {className: '+=is-active'})
		;

	var scrollAction_0 = new ScrollMagic.Scene({
		triggerElement: $section_0,
		triggerHook: 0,
		duration: '100%'
	})
		.setTween(tl)
		// .addIndicators({
		//     name: 'infoSection'
		// })
		.addTo(controller)
		;
}

/*----------  1 Section  ----------*/
function titleSection_1() {
	var tl = new TimelineMax();
	tl
		.to($title_0, $titleTime, {
			xPercent: -$titlePercent,
			autoAlpha: 0,
			// display: 'n_25',
			ease: Power2.easeInOut
		})
		.fromTo($title_1, $titleTime, {
			xPercent: $titlePercent,
			autoAlpha: 0
		}, {
			xPercent: 0,
			autoAlpha: 1,
			ease: Power2.easeInOut
		})
		;

	var scrollAction_1 = new ScrollMagic.Scene({
		triggerElement: $section_1,
		triggerHook: $triggerHook
		// duration: '100%'
	})
		.setTween(tl)
		// .addIndicators({
		//     name: 'equipmentSection'
		// })
		.addTo(controller)
		;
}

/*----------  2 Section  ----------*/
function titleSection_2() {
	var tl = new TimelineMax();
	tl
		.to($title_1, $titleTime, {
			xPercent: -$titlePercent,
			autoAlpha: 0,
			// display: 'n_25',
			ease: Power2.easeInOut
		})
		.fromTo($title_2, $titleTime, {
			xPercent: $titlePercent,
			autoAlpha: 0
		}, {
			xPercent: 0,
			autoAlpha: 1,
			ease: Power2.easeInOut
		})
		;

	var scrollAction_2 = new ScrollMagic.Scene({
		triggerElement: $section_2,
		triggerHook: $triggerHook
		// duration: '100%'
	})
		.setTween(tl)
		// .addIndicators({
		//     name: 'equipmentSection'
		// })
		.addTo(controller)
		;
}

/*=====  End of ScrollMagic Section  ======*/

/*==============================================
=                   Magnific                   =
==============================================*/

$('.section-gallery__picture').magnificPopup({
	delegate: 'a.section-gallery__link', // child items selector, by clicking on it popup will open
	type: 'iframe',
	iframe: {
		markup: '<div class="mfp-iframe-scaler">' +
			'<div class="mfp-close"></div>' +
			'<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>' +
			'</div>', // HTML markup of popup, `mfp-close` will be replaced by the close button

		patterns: {
			youtube: {
				index: 'youtube.com/', // String that detects type of video (in this case YouTube). Simply via url.indexOf(index).

				id: 'v=', // String that splits URL in a two parts, second part should be %id%
				// Or null - full URL will be returned
				// Or a function that should return %id%, for example:
				// id: function(url) { return 'parsed id'; }

				src: '//www.youtube.com/embed/%id%?autoplay=1' // URL that will be set as a source for iframe.
			},
			vimeo: {
				index: 'vimeo.com/',
				id: '/',
				src: '//player.vimeo.com/video/%id%?autoplay=1'
			},
			gmaps: {
				index: '//maps.google.',
				src: '%id%&output=embed'
			}

			// you may add here more sources

		},
		srcAction: 'iframe_src', // Templating object key. First part defines CSS selector, second attribute. "iframe_src" means: find "iframe" and set attribute "src".
	}

});

/*============  End of Magnific  =============*/

function initDesktop() {
	scrollDrag();
	// titleSection_0();
	// titleSection_1();
	// titleSection_2();
}

function initMobile() {

}

if (document.body.clientWidth > 420 || screen.width > 420) {
    initDesktop();
} else {
    initMobile();
}

})(jQuery);